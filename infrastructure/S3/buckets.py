import boto3, json
from termcolor import colored
from botocore.exceptions import ClientError

def createBucket(session, config):
    s3 = session.resource('s3')
    bucket = s3.Bucket(config['BucketName'])

    print('S3: %s' % (colored('Creating bucket \"%s\"...' % (config['BucketName']), 'yellow')))
    try:
        res = bucket.create(
            ACL=config['ACL'],
            CreateBucketConfiguration=config['CreateBucketConfiguration']
        )
        bucket.wait_until_exists()

        print('S3: %s' % (colored('Creating bucket \"%s\" created.' % (config['BucketName']), 'yellow')))

        return {
            'Location': res['Location']
        }
    except ClientError as e:
        print('S3: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))

def deleteBucket(session, config):
    s3 = session.resource('s3')
    bucket = s3.Bucket(config['BucketName'])
    try:
        print('S3: %s' % (colored('Deleting all objects of the bucket \"%s\"...' % (config['BucketName']), 'yellow')))
        bucket.objects.all().delete()
        print('S3: %s' % (colored('All objects of bucket \"%s\" deleted.' % (config['BucketName']), 'yellow')))
        print('S3: %s' % (colored('Deleting bucket \"%s\"...' % (config['BucketName']), 'yellow')))
        bucket.delete()
        print('S3: %s' % (colored('Bucket \"%s\" deleted.' % (config['BucketName']), 'yellow')))
    except ClientError as e:
        print('S3: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))
    
    

if __name__ == '__main__':
    session = boto3.Session(profile_name='gk-aws')
    with open('config.json') as file:
        config = json.load(file)
    deleteBucket(session, config['S3']['UserProfilePicturesBucket'])