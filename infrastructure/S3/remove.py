from infrastructure.S3.buckets import deleteBucket

def removeS3(session, config):
    res = input("Do you want to remove the bucket \"%s\"? [y/N] " % (config['ProfilePicturesBucketName']))
    if res == "y" or res == "Y":
        bucket = {
            "BucketName": config['ProfilePicturesBucketName']
        }
        deleteBucket(session, bucket)