from infrastructure.S3.buckets import createBucket

def deployS3(session, config):
    buckets_config = {}
    res = input("Do you want to deploy the bucket \"%s\"? [y/N] " % (config['UserProfilePicturesBucket']['BucketName']))
    if res == "y" or res == "Y":
        bucket = createBucket(session, config['UserProfilePicturesBucket'])
        buckets_config['ProfilePicturesBucketName'] = config['UserProfilePicturesBucket']['BucketName']
        buckets_config['ProfilePicturesBucketLocation'] = bucket['Location']
    return buckets_config