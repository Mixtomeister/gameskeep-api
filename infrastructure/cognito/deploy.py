from infrastructure.cognito.userPool import createUserPool

def deployCognito(session, config):
    res = input("Do you want to deploy the cognito user pool? [y/N] ")
    if res == "y" or res == "Y":
        return createUserPool(session, config)
    else:
        return {}