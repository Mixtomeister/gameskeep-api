import boto3, json, os
from termcolor import colored
from botocore.exceptions import ClientError

def createUserPool(session, config):
    cognito = session.client('cognito-idp')
    
    ACCOUNT_ID = session.client('sts').get_caller_identity().get('Account')
    REGION = session.region_name
    USER_POOL_ID = str()
    USER_POOL_NAME = str()
    USER_POOL_ARN = str()
    USER_POOL_CLIENT_NAME = str()
    USER_POOL_CLIENT_ID = str()
    USER_POOL_DOMAIN = str()

    print('Cognito: %s' % (colored('Creating the user pool...', 'yellow')))
    try:
        res = cognito.create_user_pool(
            PoolName=config['UserPool']['PoolName'],
            Policies=config['UserPool']['PoolPolicies'],
            AutoVerifiedAttributes=config['UserPool']['AutoVerifiedAttributes'],
            AliasAttributes=config['UserPool']['AliasAttributes'],
            UsernameAttributes=config['UserPool']['UsernameAttributes'],
            Schema=config['UserPool']['Schema'],
            VerificationMessageTemplate=config['UserPool']['VerificationMessageTemplate']
        )

        USER_POOL_ID = res['UserPool']['Id']
        USER_POOL_NAME = res['UserPool']['Name']
        USER_POOL_ARN = 'arn:aws:cognito-idp:%s:%s:userpool/%s' % (REGION, ACCOUNT_ID, USER_POOL_ID)

        print('Cognito: %s' % (colored('User pool created.', 'yellow')))
    except ClientError as e:
        print('Cognito: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))

    print('Cognito: %s' % (colored('Creating the user pool client...', 'yellow')))
    try:
        res = cognito.create_user_pool_client(
            UserPoolId=USER_POOL_ID,
            ClientName=config['UserPoolClient']['ClientName'],
            GenerateSecret=config['UserPoolClient']['GenerateSecret'],
            ExplicitAuthFlows=config['UserPoolClient']['ExplicitAuthFlows']
        )
        USER_POOL_CLIENT_NAME = res['UserPoolClient']['ClientName']
        USER_POOL_CLIENT_ID = res['UserPoolClient']['ClientId']

        print('Cognito: %s' % (colored('User pool client created.', 'yellow')))
    except ClientError as e:
        print('Cognito: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))
    
    print('Cognito: %s' % (colored('Creating the user pool domain...', 'yellow')))

    try:
        cognito.create_user_pool_domain(
            Domain=config['UserPoolDomain']['PrefixDomain'],
            UserPoolId=USER_POOL_ID
        )

        res = cognito.describe_user_pool(
            UserPoolId=USER_POOL_ID
        )

        USER_POOL_DOMAIN = 'https://%s.auth.%s.amazoncognito.com' % (res['UserPool']['Domain'], REGION)

        print('Cognito: %s\n' % (colored('User pool domain created.', 'yellow')))
    except ClientError as e:
        print('Cognito: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))

    userpool = {
        'UserPoolId': USER_POOL_ID,
        'UserPoolName': USER_POOL_NAME,
        'UserPoolARN': USER_POOL_ARN,
        'UserPoolClientName': USER_POOL_CLIENT_NAME,
        'UserPoolClientId': USER_POOL_CLIENT_ID,
        'UserPoolPrefixDomain': config['UserPoolDomain']['PrefixDomain'],
        'UserPoolDomain': USER_POOL_DOMAIN
    }

    print(colored('{:-^40}'.format('User Pool'), 'yellow'))
    print('%s %s' % (colored('- Pool Id:', 'yellow'), userpool['UserPoolId']))
    print('%s %s' % (colored('- Pool Name:', 'yellow'), userpool['UserPoolName']))
    print('%s %s\n' % (colored('- Pool ARN:', 'yellow'), userpool['UserPoolARN']))
    print('%s %s' % (colored('- Pool Client Name:', 'yellow'), userpool['UserPoolClientName']))
    print('%s %s\n' % (colored('- Pool Client Id:', 'yellow'), userpool['UserPoolClientId']))
    print('%s %s' % (colored('- Pool Domain:', 'yellow'), userpool['UserPoolDomain']))
    print(colored('{:-<40}'.format(''), 'yellow'))

    return userpool

def removeUserPool(session, config):
    cognito = session.client('cognito-idp')

    print('Cognito: %s' % (colored('Deleting the user pool domain...', 'yellow')))
    try:
        cognito.delete_user_pool_domain(
            Domain=config['UserPoolPrefixDomain'],
            UserPoolId=config['UserPoolId']
        )
        print('Cognito: %s' % (colored('User pool domain deleted.', 'yellow')))
    except ClientError as e:
        print('Cognito: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))
    
    print('Cognito: %s' % (colored('Deleting the user pool...', 'yellow')))
    try:
        cognito.delete_user_pool(
            UserPoolId=config['UserPoolId']
        )
        print('Cognito: %s' % (colored('User pool deleted.', 'yellow')))
    except ClientError as e:
        print('Cognito: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))