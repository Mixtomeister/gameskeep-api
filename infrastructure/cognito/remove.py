from infrastructure.cognito.userPool import removeUserPool

def removeCognito(session, config):
    res = input("Do you want to remove the cognito user pool? [y/N] ")
    if res == "y" or res == "Y":
        removeUserPool(session, config)