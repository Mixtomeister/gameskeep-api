from infrastructure.dynamodb.tables import removeTable

def removeDynamoDB(session, config):
    res = input("Do you want to remove the table \"%s\"? [y/N] " % (config['GamesTableName']))
    if res == "y" or res == "Y":
        table = {
            "TableName": config['GamesTableName']
        }
        removeTable(session, table)