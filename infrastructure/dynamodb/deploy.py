from infrastructure.dynamodb.tables import createTable

def deployDynamoDB(session, config):
    dynamo_config = {}
    res = input("Do you want to deploy the table \"%s\"? [y/N] " % (config['GamesTable']['TableName']))
    if res == "y" or res == "Y":
        table = createTable(session, config['GamesTable'])
        if table != None:
            dynamo_config.update({
                "GamesTableId": table['Table']['TableId'],
                "GamesTableName": table['Table']['TableName'],
                "GamesTableARN": table['Table']['TableArn']
            })
    return dynamo_config