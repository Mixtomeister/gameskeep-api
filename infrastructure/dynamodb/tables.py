import boto3, json, os
from termcolor import colored
from botocore.exceptions import ClientError

def createTable(session, config):
    dynamoCli = session.client('dynamodb')
    dynamoRes = session.resource('dynamodb')
    try:
        print('DynamoDB: %s' % (colored('Creating table \"%s\"...' % (config['TableName']), 'yellow')))
        dynamoCli.create_table(
            TableName=config['TableName'],
            KeySchema=config['KeySchema'],
            AttributeDefinitions=config['AttributeDefinitions'],
            ProvisionedThroughput=config['ProvisionedThroughput']
        )
        dynamoRes.Table(config['TableName']).wait_until_exists()
        res = dynamoCli.describe_table(
            TableName=config['TableName']
        )

        print('DynamoDB: %s' % (colored('Table \"%s\" created.' % (config['TableName']), 'yellow')))

        return res
    except ClientError as e:
        print('DynamoDB: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))

def removeTable(session, config):
    dynamoCli = session.client('dynamodb')
    dynamoRes = session.resource('dynamodb')
    try:
        print('DynamoDB: %s' % (colored('Deleting table \"%s\"...' % (config['TableName']), 'yellow')))
        dynamoCli.delete_table(
            TableName=config['TableName']
        )
        dynamoRes.Table(config['TableName']).wait_until_not_exists()
        print('DynamoDB: %s' % (colored('Table \"%s\" deleted.' % (config['TableName']), 'yellow')))
    except ClientError as e:
        print('DynamoDB: %s' % (colored('(%s) %s' % (e.response['Error']['Code'], e.response['Error']['Message']), 'red')))