import json, os, hashlib
import boto3
from botocore.exceptions import ClientError

def uploadProfilePicture(event, context):
    s3 = boto3.client('s3')
    cognito = boto3.client('cognito-idp')

    bucket = os.environ['PROFILE_PICTURES_BUCKET']
    user = event['requestContext']['authorizer']['claims']['email']

    url = s3.generate_presigned_url(
        ClientMethod="get_object",
        Params={
            "Bucket": bucket,
            "Key": "%s.png" % (hashlib.md5(user.encode('utf-8')).hexdigest())
        },
        ExpiresIn=3600,
        HttpMethod="put"
    )

    cognito.update_user_attributes(
        UserAttributes=[
            {
                'Name': 'picture',
                'Value': url
            }
        ],
        AccessToken=event['headers']['amz-access-token']
    )

    return {
        'statusCode': 200,
        'body': json.dumps({
            'url': url
        })
    }