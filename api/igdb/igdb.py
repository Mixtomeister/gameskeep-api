import json, os
import requests

from api.StatusResponse.HttpError import HttpError
from api.StatusResponse.HttpOK import HttpOK

API_KEY = os.environ['IGDB_API_KEY']

httpError = HttpError()
httpOK = HttpOK()

def get(event, context):
    try:
        gid = event['queryStringParameters']['gid']
    except Exception:
        return httpError.badRequest400()

    res = requests.get(
        'https://api-endpoint.igdb.com/games/{}?fields=id,name,summary,developers,publishers,genres,first_release_date,cover'.format(gid),
        headers={
            'user-key': API_KEY
        }
    )

    if res.status_code == 200:
        return httpOK.ok200(body=res.json())
    elif res.status_code == 400:
        return httpError.badRequest400()
    elif res.status_code == 404:
        return httpError.notFound404()
    

def search(event, context):
    try:
        search = event['queryStringParameters']['q']
    except Exception:
        return httpError.badRequest400()

    res = requests.get(
        'https://api-endpoint.igdb.com/games/',
        params={
            'search': search,
            'fields': 'id,name,cover'
        },
        headers={
            'user-key': API_KEY
        }
    )

    if res.status_code == 200:
        return httpOK.ok200(body=res.json())
    elif res.status_code == 400:
        return httpError.badRequest400()
    elif res.status_code == 404:
        return httpError.notFound404()