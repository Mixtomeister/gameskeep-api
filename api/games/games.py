import boto3, json, os
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr

from api.StatusResponse.HttpError import HttpError
from api.StatusResponse.HttpOK import HttpOK

dynamodb = boto3.resource('dynamodb')
TABLE_GAMES = dynamodb.Table(os.environ['GAMES_TABLE_NAME'])

httpError = HttpError()
httpOK = HttpOK()

GAMES_REQUIREMENTS = {
    "PENDING": [
        "gid",
        "platform"
    ],
    "PLAYING": [
        "gid",
        "platform",
        "startdate"
    ],
    "FINISHED": [
        "gid",
        "platform",
        "startdate",
        "enddate",
        "rate"
    ]
}

def post(event, context):
    try:
        game = json.loads(event['body'])
    except Exception:
        return httpError.badRequest400()

    if _testGame(game) == False:
        return httpError.badRequest400()

    game['uid'] = event['requestContext']['authorizer']['claims']['sub']

    
    try:
        TABLE_GAMES.put_item(
            Item=game
        )
    except ClientError as e:
        return httpError.notFound404(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )

    game.pop('uid', None)

    return httpOK.created201(body=game)
    
def getAll(event, context):
    try:
        sub = event['requestContext']['authorizer']['claims']['sub']
    except Exception:
        return httpError.forbidden403()
    
    try:
        res = TABLE_GAMES.query(
            KeyConditionExpression=Key('uid').eq(sub) 
        )
    except ClientError:
        return httpOK.ok200(body=[])
    
    try:
        return httpOK.ok200(body=res['Items'])
    except Exception:
        return httpError.notFound404()

def get(event, context):
    try:
        sub = event['requestContext']['authorizer']['claims']['sub']
        gid = event['pathParameters']['gid']
    except Exception:
        return httpError.badRequest400()
    
    try:
        res = TABLE_GAMES.get_item(
            Key={
                "uid": sub,
                "gid": gid
            }
        )
    except ClientError as e:
        return httpError.notFound404(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )
    
    try:
        game = res['Item']
        game.pop('uid', None)
        return httpOK.ok200(body=game)
    except Exception:
        return httpError.notFound404()

def delete(event, context):
    try:
        gid = event['pathParameters']['gid']
        TABLE_GAMES.delete_item(
            Key={
                "uid": event['requestContext']['authorizer']['claims']['sub'],
                "gid": gid
            }
        )
    except Exception:
        return httpError.badRequest400()

    return httpOK.ok200()

def _testGame(game):
    try:
        keys = list(game.keys())
        if game['status'] == "PENDING" or game['status'] == "PLAYING" or game['status'] == "FINISHED":
            for req in GAMES_REQUIREMENTS[game['status']]:
                keys.index(req)
                if game[req] == "":
                    return False
            return True
        else:
            return False
    except Exception:
        return False