import json


class HttpError():

    def __init__(self):
        self.headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials" : True,
            "Content-Type": "application/json"
        }

    def badRequest400(self, error='Bad Request', message='Bad Request', headers={}):
        headers.update(self.headers)
        return {
            'statusCode': 400,
            'body': json.dumps({
                'Error': error,
                'Message': message
            }),
            'headers': headers
        }
    
    def unauthorized401(self, error='Unauthorized', message='Unauthorized', headers={}):
        headers.update(self.headers)
        return {
            'statusCode': 401,
            'body': json.dumps({
                'Error': error,
                'Message': message
            }),
            'headers': headers
        }

    def forbidden403(self, error='Forbidden', message='Forbidden', headers={}):
        headers.update(self.headers)
        return {
            'statusCode': 403,
            'body': json.dumps({
                'Error': error,
                'Message': message
            }),
            'headers': headers
        }

    def notFound404(self, error='Not Found', message='Not Found', headers={}):
        headers.update(self.headers)
        return {
            'statusCode': 404,
            'body': json.dumps({
                'Error': error,
                'Message': message
            }),
            'headers': headers
        }