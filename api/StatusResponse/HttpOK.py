import json


class HttpOK():

    def __init__(self):
        self.headers = {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials" : True,
            "Content-Type": "application/json"
        }

    def ok200(self, body=None, headers={}):
        headers.update(self.headers)
        return {
            "statusCode": 200,
            "body": json.dumps(body),
            "headers": headers
        }

    def created201(self, body=None, headers={}):
        headers.update(self.headers)
        return {
            "statusCode": 201,
            "body": json.dumps(body),
            "headers": headers
        }