import json, os
import boto3
from botocore.exceptions import ClientError

from api.StatusResponse.HttpError import HttpError
from api.StatusResponse.HttpOK import HttpOK

cognito = boto3.client('cognito-idp')

CLIENT_ID = os.environ['CLIENT_ID']

httpError = HttpError()
httpOK = HttpOK()

def signup(event, context):
    try:
        user = json.loads(event['body'])
        cognito.sign_up(
        ClientId=CLIENT_ID,
        Username=user['username'],
        Password=user['password'],
        UserAttributes=[
                {
                    'Name': 'email',
                    'Value': user['email']
                }
            ]
        )

        return httpOK.created201()
    except ClientError as e:
        return httpError.badRequest400(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )

def login(event, context):
    try:
        user = json.loads(event['body'])
        username = user['username']
        password = user['password']
    except Exception:
        return httpError.badRequest400()

    try:
        res = cognito.initiate_auth(
            ClientId=CLIENT_ID,
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': username,
                'PASSWORD': password
            }
        )

        user = cognito.get_user(
            AccessToken=res['AuthenticationResult']['AccessToken']
        )
        
        user.pop('ResponseMetadata', None)

        return httpOK.ok200(
            body= {
                "Auth": res['AuthenticationResult'],
                "User": user
            }
        )
    except ClientError as e:
        return httpError.unauthorized401(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )

def refresh(event, context):
    try:
        body = json.loads(event['body'])
        token = body['refreshToken']
    except Exception:
        return httpError.badRequest400()
    try:
        res = cognito.initiate_auth(
            ClientId=CLIENT_ID,
            AuthFlow='REFRESH_TOKEN_AUTH',
            AuthParameters={
                'REFRESH_TOKEN': token
            }
        )

        user = cognito.get_user(
            AccessToken=res['AuthenticationResult']['AccessToken']
        )
        
        user.pop('ResponseMetadata', None)

        return httpOK.ok200(
            body= {
                "Auth": res['AuthenticationResult'],
                "User": user
            }
        )
    except ClientError as e:
        return httpError.unauthorized401(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )

def forgotPassword(event, context):
    try:
        user = json.loads(event['body'])
        cognito.forgot_password(
            ClientId=CLIENT_ID,
            Username=user['recoverEmail']
        )

        return httpOK.created201()
    except ClientError as e:
        return httpError.badRequest400(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )

def confirmForgotPassword(event, context):
    try:
        user = json.loads(event['body'])
        cognito.confirm_forgot_password(
            ClientId=CLIENT_ID,
            Username=user['recoverEmail'],
            ConfirmationCode=user['ConfirmationCode'],
            Password=user['Password']
        )

        return httpOK.created201()
    except ClientError as e:
        return httpError.badRequest400(
            error=e.response['Error']['Code'], 
            message=e.response['Error']['Message']
        )
