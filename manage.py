#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import boto3, json, os, sys

from infrastructure.cognito.deploy import deployCognito
from infrastructure.cognito.remove import removeCognito
from infrastructure.S3.deploy import deployS3
from infrastructure.S3.remove import removeS3
from infrastructure.dynamodb.deploy import deployDynamoDB
from infrastructure.dynamodb.remove import removeDynamoDB
from api.deploy import deployServerlessAPI
from api.remove import removeServerlessAPI

session = boto3.Session(profile_name='gk-aws')

def deploy():
    filename = ".aws/environment.json"
    environment_config = {}
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    try:
        with open(filename, 'r') as file:
            environment_config = json.load(file)
    except IOError:
        pass

    with open('config.json') as file:
        config = json.load(file)

    environment_config.update(deployCognito(session, config['Cognito']))
    with open(filename, 'w') as envfile:
        json.dump(environment_config, envfile, indent=4)
    environment_config.update(deployS3(session, config['S3']))
    with open(filename, 'w') as envfile:
        json.dump(environment_config, envfile, indent=4)
    environment_config.update(deployDynamoDB(session, config['DynamoDB']))
    with open(filename, 'w') as envfile:
        json.dump(environment_config, envfile, indent=4)

    deployServerlessAPI(config)

def remove():
    if os.path.isfile('.aws/environment.json'):
        with open('.aws/environment.json') as file:
            config = json.load(file)
        
        removeCognito(session, config)
        removeS3(session, config)
        removeDynamoDB(session, config)
        removeServerlessAPI(config)

    else:
        print("Error: No aws config found")


if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "deploy":
        deploy()
    elif len(sys.argv) > 1 and sys.argv[1] == "remove":
        remove()
    else:
        print("Error: Invalid arguments.")